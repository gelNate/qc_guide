/*jshint esversion: 6 */
/**
 * RGBToHex
 *
 * Takes an RGB value and converts it to Hexadecimal
 * param {string} RGB Colour
 * return {string} Returns hexadecimal colour code
 */
function RGBToHex(rgb) {
    // Get correct separator
    let sep = rgb.indexOf(",") > -1 ? "," : " ";
    // Change from rgb() to array [r,g,b]
    rgb = rgb.substr(4).split(")")[0].split(sep);

    // convert % to 0–255
    for (let R in rgb) {
        let r = rgb[R];
        if (r.indexOf("%") > -1)
            rgb[R] = Math.round(r.substr(0, r.length - 1) / 100 * 255);
    }

    let r = (+rgb[0]).toString(16),
        g = (+rgb[1]).toString(16),
        b = (+rgb[2]).toString(16);

    if (r.length === 1) {
        r = "0" + r;
    }
    if (g.length === 1) {
        g = "0" + g;
    }
    if (b.length === 1) {
        b = "0" + b;
    }

    return "#" + r + g + b;
}

/**
 * RGBAToHexA
 *
 * Takes an RGBA value and converts it to an object containing hexadecimal codes, with and without alpha value, as well as the opacity level.
 * param {string} RGBA Code
 * return {json} JSON Object containing hexadecimal code, hexadecimal with alpha code and opacity level
 */
function RGBAToHexA(rgba) {

    let sep  = rgba.indexOf(",") > -1 ? "," : " ";
    
    rgba = rgba.substr(5).split(")")[0].split(sep);

    // strip the slash if using space-separated syntax
    if (rgba.indexOf("/") > -1) {
        rgba.splice(3, 1);
    }

    for (let R in rgba) {
        let r = rgba[R];
        if (r.indexOf("%") > -1) {
            let p = r.substr(0, r.length - 1) / 100;

            if (R < 3) {
                rgba[R] = Math.round(p * 255);
            } else {
                rgba[R] = p;
            }
        }
    }

    let r = (+rgba[0]).toString(16),
        g = (+rgba[1]).toString(16),
        b = (+rgba[2]).toString(16),
        a = Math.round(+rgba[3] * 255).toString(16);

    if (r.length === 1) {
        r = "0" + r;
    }
    if (g.length === 1) {
        g = "0" + g;
    } 
    if (b.length === 1) {
        b = "0" + b;
    }
    if (a.length === 1) {
        a = "0" + a;
    }

    var hexItem = {
        'hexCode': "#" + r + g + b,
        'hexaCode': "#" + r + g + b + a,
        'opacity': rgba[3]
    };
    return hexItem;
}

/**
 * hexToRGB
 *
 * Takes a hexadecimal value and converts it to RGB
 * param {string} Hexadecimal Colour Code
 * param {boolean} Are values require in percentage format or not? (True / False)
 * return {string} Returns RGB colour code
 */
function hexToRGB(h, isPct) {
    let r = 0,
        g = 0,
        b = 0;

    isPct = isPct === true;

    // 3 digits
    if (h.length === 4) {
        r = "0x" + h[1] + h[1];
        g = "0x" + h[2] + h[2];
        b = "0x" + h[3] + h[3];

        // 6 digits
    } else if (h.length === 7) {
        r = "0x" + h[1] + h[2];
        g = "0x" + h[3] + h[4];
        b = "0x" + h[5] + h[6];
    }
    if (isPct) {
        r = +(r / 255 * 100).toFixed(1);
        g = +(g / 255 * 100).toFixed(1);
        b = +(b / 255 * 100).toFixed(1);
    }
    return "rgb(" + (isPct ? r + "%," + g + "%," + b + "%" : +r + "," + +g + "," + +b) + ")";
}

/**
 * hexAToRGBA
 *
 * Takes an RGBA value and converts it to an object containing hexadecimal codes, with and without alpha value, as well as the opacity level.
 * param {string} Hexadecimal with Alpha Code
 * param {boolean} Are values require in percentage format or not? (True / False)
 * return {json} JSON Object containing RGB code, RGBA Code and opacity level
 */
function hexAToRGBA(h, isPct) {
    let r = 0,
        g = 0,
        b = 0,
        a = 1;
    isPct = isPct === true;

    if (h.length === 5) {
        r = "0x" + h[1] + h[1];
        g = "0x" + h[2] + h[2];
        b = "0x" + h[3] + h[3];
        a = "0x" + h[4] + h[4];

    } else if (h.length === 9) {
        r = "0x" + h[1] + h[2];
        g = "0x" + h[3] + h[4];
        b = "0x" + h[5] + h[6];
        a = "0x" + h[7] + h[8];
    }
    a = +(a / 255).toFixed(3);
    if (isPct) {
        r = +(r / 255 * 100).toFixed(1);
        g = +(g / 255 * 100).toFixed(1);
        b = +(b / 255 * 100).toFixed(1);
        a = +(a * 100).toFixed(1);
    }
    var rgbaItem = {
        'rgbaCode': "rgba(" + (isPct ? r + "%," + g + "%," + b + "%" + "," + a : +r + "," + +g + "," + +b + "," + a) + ")",
        'opacity': a,
        'rgbCode': "rgb(" + (isPct ? r + "%," + g + "%," + b + "%" + "," + a : +r + "," + +g + "," + +b) + ")"
    };
    return rgbaItem;
}

/**
 * processColor
 *
 * Takes an RGBA value and converts it to an object containing hexadecimal codes, with and without alpha value, as well as the opacity level.
 * param {string} Color code
 * return {html} HTML code with colour details including RGB code, RGBA Code, Hexadecimal Code, Hexadecimal with Alpha Code and Opacity Level.
 * The main item will be shown in Hexadecimal with a hover over description for the rest.
 */
function processColor(color) {

    const regexString = /^(?:#(?:[A-Fa-f0-9]{3}){1,2}|(?:rgb[(](?:\s*0*(?:\d\d?(?:\.\d+)?(?:\s*%)?|\.\d+\s*%|100(?:\.0*)?\s*%|(?:1\d\d|2[0-4]\d|25[0-5])(?:\.\d+)?)\s*(?:,(?![)])|(?=[)]))){3}|hsl[(]\s*0*(?:[12]?\d{1,2}|3(?:[0-5]\d|60))\s*(?:\s*,\s*0*(?:\d\d?(?:\.\d+)?\s*%|\.\d+\s*%|100(?:\.0*)?\s*%)){2}\s*|(?:rgba[(](?:\s*0*(?:\d\d?(?:\.\d+)?(?:\s*%)?|\.\d+\s*%|100(?:\.0*)?\s*%|(?:1\d\d|2[0-4]\d|25[0-5])(?:\.\d+)?)\s*,){3}|hsla[(]\s*0*(?:[12]?\d{1,2}|3(?:[0-5]\d|60))\s*(?:\s*,\s*0*(?:\d\d?(?:\.\d+)?\s*%|\.\d+\s*%|100(?:\.0*)?\s*%)){2}\s*,)\s*0*(?:\.\d+|1(?:\.0*)?)\s*)[)])$/gm;
    var type = 0;
    var correctColourPattern = regexString.exec(color);
    if (!correctColourPattern) {
        return color;
    }

    var hexCol  = false,
        rgbCol  = false,
        rgbaCol = false,
        hexaCol = false,
        opacity = 1
        hasColor = false;


    if (color.includes("#")) {
        type = 1;
        hexCol = color;
        rgbCol = hexToRGB(color);
        hasColor = true;
    }
    if (color.includes("rgba(")) {
        type = 3;
        rgbaCol = color;
        hasColor = true;

        var hexaObject = RGBAToHexA(color);
        hexaCol = hexaObject.hexaCode;
        hexCol = hexaObject.hexCode;
        opacity = hexaObject.opacity;
        rgbCol = hexToRGB(hexCol);
    }
    if (color.includes("rgb(")) {
        type = 2;
        rgbCol = color;
        hexCol = RGBToHex(color);
        hasColor = true;
    }
    
    if (!hasColor) return color;

    var hex = color,
        rgbOutput = '',
        red = parseInt(hex[1] + hex[2], 16),
        green = parseInt(hex[3] + hex[4], 16),
        blue = parseInt(hex[5] + hex[6], 16);

    if (hex.length > 7) {
        var alpha = parseInt(hex[7] + hex[8]);
        rgbOutput = 'rgba(' + red + ',' + green + ',' + blue + ',' + alpha + ')';
    } else {
        var alpha = false;
        rgbOutput = 'rgb(' + red + ',' + green + ',' + blue + ')';
    }

    var color = {
        'hex': hexCol,
        'rgb': rgbCol,
        'rgba': rgbaCol,
        'hexA': hexaCol,
        'opacity': opacity
    };

    var cBox = `<div style="display: inline-block;" 
			alt="RGB Value: ${color.rgb} 
RGBA Value: ${color.rgba} 
Opacity Value: ${color.opacity}" 
			title="RGB Value: ${color.rgb} 
RGBA Value: ${color.rgba} 
Opacity Value: ${color.opacity}">
				<span class="cblk" style="background-color:${color.hex}"></span> ${color.hex}</div>`;
    return cBox;
}
