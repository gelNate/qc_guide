/*jshint esversion: 6 */


/**
 * debounce
 * 
 * Prevents function from running while event listener is running
 * param {function} function to debounce
 * return {void}
 */
function debounce(func, time = 100) {
    var timer;
    return function (event) {
        if (timer) clearTimeout(timer);
        timer = setTimeout(func, time, event);
    };
}

// Enable skip links for contents
document.addEventListener('click', function (event) {
    // Prevent the processing of the .content-link
    if (event.target.matches('.content-link')) {
        event.preventDefault();
        return;
    }
    // If a link is clicked that is not a .skip-link 
    // then let it pass
    if (!event.target.matches('.skip-link')) return;

    event.preventDefault();
    var elId = event.target.getAttribute('href'),
        el = document.getElementById(elId.replace("#", ""));
    // Scroll the el into view
    el.scrollIntoView();
    // change the URL to the HASH ID
    window.location.hash = elId;
}, false);

// ReCompute styles on window resize
window.addEventListener("resize", debounce(function (e) {
    // reset the counter
    computeItemX = 0;
    // recompute the styles
    computeStyles();
}));

window.addEventListener("load", debounce(function (e) {
    // compute styles
    computeStyles();
    // build table of contents links 
    buildContents();
}));