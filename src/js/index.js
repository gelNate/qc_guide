/**
 * QC Guide
 * 
 * A QC framework for developers to run tests and present important 
 * information about DOM elements as they are rendered on a browser 
 * at runtime
 * 
 * @author Nathanael McMillan
 * @author Justin Slabbert
 * @version 0.1.0-pl
 *  
 */

/*jshint esversion: 6 */

const bodyDoc = getComputedStyle(document.body);
const guideConfig = {
    xs: 575,
    sm: 576,
    md: 768,
    lg: 992,
    xl: 1200,
    xxl: 1400,
    xxxl: 1660
};
// For debugging and testing 
const computePerformance = {
    start_time: 0,
    end_time: 0,
    process_microtime: 0,
    process_seconds: 0
};
// For IDXing items
var computeItemX = 0;

// Import for CodeKit 
// @codekit-append "_compute_colors.function.js"
// @codekit-append "_compute_styles.functions.js"
// @codekit-append "_debug.js"
// @codekit-append "_event.listeners.js"