/*jshint esversion: 6 */
// ---------------------------------
// Testing and debugging this script 
// ---------------------------------

/**
 * computeTime
 * 
 * Calculates the time taken for script to compute all elements 
 * on page in microseconds
 * @param {number} Interval to return. Default: 1000 (seconds)
 * @return {number} 
 */
function computeTime(interval = 1000) {
    computePerformance.process_microtime = computePerformance.end_time - computePerformance.start_time;
    return computePerformance.process_microtime / interval;
}