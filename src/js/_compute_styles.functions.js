/*jshint esversion: 6 */
/**
 * getBreakpoint
 * 
 * Returns the breakpoint of the current screen width
 * @return {sting}
 */
function getBreakpoint() {
    var ww = window.innerWidth; // get the width of the window;

    if (ww <= (guideConfig.sm - 1)) {
        bp = 'xs';
    }

    if (ww >= guideConfig.sm && ww <= (guideConfig.md - 1)) {
        bp = 'sm';
    }

    if (ww >= guideConfig.md && ww <= (guideConfig.lg - 1)) {
        bp = 'md';
    }

    if (ww >= guideConfig.lg && ww <= (guideConfig.xl - 1)) {
        bp = 'lg';
    }

    if (ww >= guideConfig.xl && ww <= (guideConfig.xxl - 1)) {
        bp = 'xl';
    }

    if (ww >= guideConfig.xxl && ww <= (guideConfig.xxxl - 1)) {
        bp = 'xxl';
    }

    if (ww >= guideConfig.xxxl) {
        bp = 'xxxl';
    }
    return bp;
}
/** 
 * assert
 * 
 * @param {string} Value to test is correct 
 * @param {object} Object that contains the breakpoints and the values to test against 
 * @return {bool}
 */
function Assert(value, tests) {
    // Get the current width of the screen 	
    var bp = getBreakpoint();
    // see if we have this breakpoint in the assertation object
    if (bp in tests) {
        if (value !== tests[bp]) {
            return false;
        } else {
            return true;
        }
    } else {
        // this BP has not been defined, try and figure out if the value is correct based on the 
        // the objects properties 
        var testArray = [],
            goal = guideConfig[bp];

        for (const [key, value] of Object.entries(tests)) {
            testArray.push(guideConfig[key]);
        }
        console.log('testArray', testArray);

        var closest = testArray.reverse().find(e => e <= goal);
        var closeKey = getKeyByValue(guideConfig, closest);
        if (value !== tests[closeKey]) {
            return false;
        } else {
            return true;
        }
    }

    return null;
}

function getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
}
/**
 * getVar 
 * param {string} CSS Var
 */
function getVar(cVar) {
    document.write(bodyDoc.getPropertyValue(cVar));
}

/**
 * getInfoAdvanced 
 * 
 * Returns computed styles of element
 * param {string} Element selector i.e. #id or .className or #id .classname [name='fname']
 * param {string} What computed information you want to return 
 * param {string|null} If not null, returns computed info on pseudo element i.e. before, after
 */
function getInfoAdvanced(elId, what, pseudoEl) {
    var el = document.querySelector(elId),
        info = window.getComputedStyle(el);
    if (pseudoEl !== null) {
        info = window.getComputedStyle(el, '::' + pseudoEl);
    }
    computeItemX++;
    return info[what];
}

/**
 * computeStyles 
 * 
 * Loops through all elements with data-compute="true" and returns a list of 
 * computed styles requested in data-styles
 * return {string} List of all computed CSS styles
 */
function computeStyles() {
    computePerformance.start_time = Date.now();
    // Grab all elements with data-compute=true
    var loopItems = document.querySelectorAll('[data-compute]');
    // Loop through items
    for (i = 0; i < loopItems.length; ++i) {
        // set vars based on data attrs
        var loopItemId = loopItems[i].getAttribute('data-id'),
            styleItems = loopItems[i].getAttribute('data-styles'),
            showLabel = loopItems[i].getAttribute('data-label') ? false : true,
            assert = JSON.parse(loopItems[i].getAttribute('data-assert')) ?? false,
            pseudo = loopItems[i].getAttribute('data-pseudo'),
            htmlItems = styleItems.split(","),
            computedArr = [];

        for (hI = 0; hI < htmlItems.length; ++hI) {

            // Replace 
            var itemKey = htmlItems[hI],
                replacedText = itemKey.replace(/([A-Z])/g, " $1"),
                keyText = replacedText.charAt(0).toUpperCase() + replacedText.slice(1),
                psRaw = getInfoAdvanced(loopItemId, htmlItems[hI], pseudo),
                ps = processColor(psRaw),
                assertion = '';


            if (assert !== false) {
                // check to see if we have this key in our object 
                if (itemKey in assert) {
                    if (Assert(ps, assert[itemKey])) {
                        assertion = `<span class="text-success">passing</span>`;
                    } else if (Assert(ps, assert[itemKey]) !== null && !Assert(ps, assert[itemKey])) {
                        assertion = `<span class="text-danger">failing</span>`;
                    }

                }
            }

            if (showLabel) {
                computedArr.push(`<li class="list-group-item">
							<span><strong>${keyText}: </strong> ${ps} </span> ${assertion}
							<span class="text-small">${computeItemX}</span>
						</li>`);
            } else {
                computedArr.push(`<span title="ID: ${computeItemX}">${ps}</span>`);
            }
        }
        var joinItems = computedArr.join("");

        loopItems[i].innerHTML = `<ul class="list-group list-group-flush">${joinItems}</ul>`;
    }
    computePerformance.end_time = Date.now();
}




/**
 *	End of colour conversion functions - JUSTIN
 */

function buildContents() {
    var items = document.querySelectorAll('.content-link'),
        contents = [];
    var colorObject = processColor('testrgba(255, 123, 0, 0.5)');

    for (i = 0; i < items.length; i++) {
        var link = items[i].getAttribute('href'),
            text = items[i].innerHTML;

        contents.push(`<a class="list-group-item list-group-item-action skip-link" href="${link}">${text}</a>`);
    }
    document.getElementById('contentsNav').innerHTML = contents.join("");
}